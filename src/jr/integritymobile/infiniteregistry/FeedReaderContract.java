package src.jr.integritymobile.infiniteregistry;

import android.content.Context;
import android.database.sqlite.*;
import android.provider.*;


public class FeedReaderContract {

	public FeedReaderContract() {}
	
	public static abstract class feedEntry implements BaseColumns{
		//All statics for the items table
		public static final String ITEMS_TABLE = "items";
		public static final String ITEMS_ID = "item_id";
		public static final String ITEMS_LIST = "list_id";
		public static final String ITEMS_UPC ="upc";
		public static final String ITEMS_NAME = "name";
		public static final String ITEMS_PRICE = "price";
		public static final String ITEMS_DATE = "date_add";
		public static final String ITEMS_QTY = "qty";
		
		//All statics for the items_bought table
		public static final String BOUGHT_TABLE = "items_bought";
		public static final String BOUGHT_ID = "item_id";
		public static final String BOUGHT_QTY = "qty_bought";
		public static final String BOUGHT_DATE = "date_modified";
		
		//All statics for the lists table
		public static final String LISTS_TABLE = "lists";
		public static final String LISTS_ID = "list_id";
		public static final String LISTS_NAME = "list_name";
	}
	
	String createStmt = "CREATE TABLE ";
	String deleteStmt = "DROP TABLE IF EXISTS ";
	
	final String COMMA_SEP = ",";
	final String TEXT_TYPE = " varchar";
	final String INT_TYPE = " int";
	final String FLOAT_TYPE = " float";
	final String DATE_TYPE = " date";
	
	final String SQL_CREATE_ITEMS_TBL = createStmt+feedEntry.ITEMS_TABLE+" ("+
					feedEntry.ITEMS_ID + " INTEGER PRIMARY KEY,"+
					feedEntry.ITEMS_LIST + INT_TYPE+COMMA_SEP+
					feedEntry.ITEMS_NAME + TEXT_TYPE + COMMA_SEP+
					feedEntry.ITEMS_PRICE + FLOAT_TYPE + COMMA_SEP+
					feedEntry.ITEMS_QTY + INT_TYPE + COMMA_SEP +
					feedEntry.ITEMS_UPC + TEXT_TYPE+COMMA_SEP+
					feedEntry.ITEMS_DATE + DATE_TYPE + COMMA_SEP;
			
	final String SQL_CREATE_BOUGHT_TBL = createStmt+feedEntry.BOUGHT_TABLE+" ("+
					feedEntry.BOUGHT_ID + " INTEGER PRIMARY KEY,"+
					feedEntry.BOUGHT_QTY + INT_TYPE + COMMA_SEP +
					feedEntry.BOUGHT_DATE + DATE_TYPE + COMMA_SEP;

	final String SQL_CREATE_LISTS_TBL = createStmt+feedEntry.LISTS_TABLE+" ("+
					feedEntry.LISTS_ID + " INTEGER PRIMARY KEY,"+
					feedEntry.LISTS_NAME + TEXT_TYPE + COMMA_SEP;
	
	final String SQL_DELETE_ITEMS_TBL = deleteStmt+feedEntry.ITEMS_TABLE;
	final String SQL_DELETE_BOUGHT_TBL = deleteStmt+feedEntry.BOUGHT_TABLE;
	final String SQL_DELETE_LISTS_TBL = deleteStmt+feedEntry.LISTS_TABLE;
	
	public class dbHelper extends SQLiteOpenHelper {

		public static final int DATABASE_VERSION = 1;
		public static final String DATABASE_NAME = "registry.db";
		
		public dbHelper (Context context){
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db){
			db.execSQL(SQL_CREATE_ITEMS_TBL);
			db.execSQL(SQL_CREATE_BOUGHT_TBL);
			db.execSQL(SQL_CREATE_LISTS_TBL);
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer){}
		
		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVer, int newVer){}		
		
	}
	
}
