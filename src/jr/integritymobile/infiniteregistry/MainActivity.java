package src.jr.integritymobile.infiniteregistry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener {

	private Button createBtn, listBtn;
	private TextView welcomeTxt;
	private String uid;
	public final static String EXTRA_MESSAGE = "jr.integritymobile.infiniteregistry.MESSAGE";
	public final static String EXTRA_UID = "jr.integritymobile.infiniteregistry.UID";
	public final static String DEBUG_TAG = "infiniteregistry.DEBUG";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Check for network status - if no - alert user
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
				
			boolean newDevice;
			String user = "";

			SharedPreferences pref = this.getPreferences(Context.MODE_PRIVATE);
			String username = pref.getString(getString(R.string.username), "");
			uid = pref.getString(getString(R.string.uid), "");
			Intent intentStart = getIntent();
			if(!username.equals("")){
				Log.d(DEBUG_TAG, "pulled uid from file");
				newDevice = false;
				user = username;
				if(uid.equals("")){
					alert("UID Does not exist!!!");
				}
			}else if(intentStart.getStringExtra(NewDevice.EXTRA_UID) instanceof String){
				Log.d(DEBUG_TAG, "pulled uid from intent");
				newDevice = false;
				user = intentStart.getStringExtra(NewDevice.EXTRA_USER);
				uid = intentStart.getStringExtra(NewDevice.EXTRA_UID);
				SharedPreferences.Editor editor = pref.edit();
				editor.putString(getString(R.string.uid), uid);
				editor.putString(getString(R.string.username), user);
				editor.commit();
			} else {
				newDevice = true;
			}
			// set custom font for button text
			Typeface font = Typeface.createFromAsset(getAssets(),"ambassador_script.ttf");
			
			// Also instantiate any db connectors
			// FeedReaderContract.dbHelper dbHelper = new
			// FeedReaderContract.dbHelper(getContext());
			
			if (newDevice) {
				Intent intent = new Intent(this, NewDevice.class);
				startActivity(intent);
			} else {
				setContentView(R.layout.activity_main);
			
				createBtn = (Button) findViewById(R.id.create_button);
				listBtn = (Button) findViewById(R.id.list_button);
			
				createBtn.setTypeface(font);
				listBtn.setTypeface(font);
			
				welcomeTxt = (TextView) findViewById(R.id.welcomeBanner);
			
				createBtn.setOnClickListener(this);
				listBtn.setOnClickListener(this);
				if(!user.equals(""))
					welcomeTxt.setText("Hi, " + user);
			}
		} else {
			alert("The Infinite Registry requires an internet/data connection");
		}
	}

	@Override
	public void onClick(View v) {
		// respond to clicks
		if (v.getId() == R.id.create_button) {
			Intent intent = new Intent(this, Create.class);
			intent.putExtra(EXTRA_UID, uid);
			startActivity(intent);
		} else if (v.getId() == R.id.list_button) {	
			Intent intent = new Intent(this, List.class);
			intent.putExtra(EXTRA_UID, uid);
			startActivity(intent);
		} 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	protected void alert(String msg) {
		Toast toast = Toast.makeText(getApplicationContext(), msg,
				Toast.LENGTH_SHORT);
		toast.show();
	}
}
