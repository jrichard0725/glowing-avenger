package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Create extends Activity implements OnClickListener {

	private Button submitBtn;
	private ProgressDialog progress;
	private TextView error;
	private String uid, res_val;
	public final static String EXTRA_USER = "jr.integritymobile.infiniteregistry.USER";
	public final static String EXTRA_UID = "jr.integritymobile.infiniteregistry.UID";
	public final static String EXTRA_LISTID = "jr.integritymobile.infiniteregistry.LISTID";

	// private TextView formatTxt, contentTxt, lengthTxt, listTxt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Check for network status - if no - alert user
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			
			//get uid
			Intent intentStart = getIntent();
			uid = intentStart.getStringExtra(MainActivity.EXTRA_UID);
			if(uid.equals(""))
				new Helper().alert("UID Does not exist");

			setContentView(R.layout.activity_create);

			Typeface font = Typeface.createFromAsset(getAssets(),
					"ambassador_script.ttf");
			submitBtn = (Button) findViewById(R.id.submit);
			submitBtn.setTypeface(font);

			submitBtn.setOnClickListener(this);
			// Show the Up button in the action bar.
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				setupActionBar();
			}
		} else {
			new Helper()
					.alert("The Infinite Registry requires an internet/data connection");
		}
	}

	@Override
	public void onClick(View v) {
		// respond to clicks
		Context context = (Context) this;
		progress = new ProgressDialog(context);
		error = (TextView) findViewById(R.id.error);
		
		EditText nameRaw = (EditText) findViewById(R.id.reg_name);
		EditText fnameRaw = (EditText) findViewById(R.id.reg_forFirst);
		EditText lnameRaw = (EditText) findViewById(R.id.reg_forLast);
		EditText descRaw = (EditText) findViewById(R.id.reg_description);
		EditText locRaw = (EditText) findViewById(R.id.reg_loc);
		
		String name = nameRaw.getText().toString();
		String fname = fnameRaw.getText().toString();
		String lname = lnameRaw.getText().toString();
		String desc = descRaw.getText().toString();
		String loc = locRaw.getText().toString();

		String myurl = buildURL(name, fname, lname, desc, loc);
		try {
			new Helper().execute(myurl);
		} catch (Exception e) {}
	}

	private String buildURL(String name, String fname, String lname, String desc, String loc) {
		String url ="";
		try{
			name = URLEncoder.encode(name, "UTF-8");
			fname = URLEncoder.encode(fname, "UTF-8");
			lname = URLEncoder.encode(lname, "UTF-8");
			desc = URLEncoder.encode(desc, "UTF-8");
			loc = URLEncoder.encode(loc, "UTF-8");
		} catch (Exception e){}
		url = "http://www.theinfiniteregistry.com/insert.php?type=list&uid="+ uid + "&name=" + name	+ "&fname="	+ fname	+ "&lname="	+ lname	+ "&desc=" + desc + "&loc=" + loc;
		return url;
	}

	private void redirect(String listid_val) {
		if (listid_val.indexOf("username") > -1 || listid_val.indexOf("email") > -1) {
			error.setText(listid_val);
		} else {
			Intent intent = new Intent(this, List.class);
			intent.putExtra(EXTRA_UID, uid);
			startActivity(intent);
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class Helper extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Creating new registry...");
			progress.setMessage("Creating...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {

			// params comes from the execute() call: params[0] is the url.
			try {
				return get(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			res_val = result;
			redirect(res_val);
		}

		protected void alert(String msg) {
			Toast toast = Toast.makeText(getApplicationContext(), msg,
					Toast.LENGTH_SHORT);
			toast.show();
		}

		@SuppressWarnings("rawtypes")
		private String get(String myurl) throws IOException {
			String error_msg = "", json = "";
			InputStream is = null;
			// Only display the first len characters of the retrieved
			// web page content.
			int len = 500;

			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);

				conn.connect();
				is = conn.getInputStream();

				json = (String) readIt(is, len);
				if (json.indexOf("error") > -1) {
					String error = "";
					if (json.indexOf("email") > -1) {
						error = "email address";
					} else if (json.indexOf("username") > -1) {
						error = "username";
					}
					return "That " + error	+ " already exists. Please try again.";
				} else {
					Gson gson = new Gson();
					jsonType id = gson.fromJson(json, jsonType.class);
					conn.disconnect();

					if (is != null) {
						is.close();
					}
					return id.listid;
				}
			} catch (Exception e) {
				Class type = e.getClass();
				error_msg = "Error Type: " + type.getName() + "\nMessage: "	+ e.getLocalizedMessage() + "\nCause:" + e.getCause();
				return error_msg;
				// Makes sure that the InputStream is closed after the app is
				// finished using it.
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		// Reads an InputStream and converts it to a String.
		public String readIt(InputStream stream, int len) throws IOException,
				UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			String values = new String(buffer);
			// Limit the result to only the json
			int startIndex = values.indexOf("{");
			int endIndex = values.indexOf("}");
			values = values.substring(startIndex, endIndex + 1);
			return (String) values;
		}
		
		public class jsonType {
			public String listid;
		}
	}

}
