package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import src.com.google.zxing.integration.android.IntentIntegrator;
import src.com.google.zxing.integration.android.IntentResult;

public class Items extends Activity implements OnClickListener{
	
	private Button scanBtn;
	private TextView listTxt;
	private String listid;
	private ProgressDialog progress;
    private Utility util;
    private JSONObject json;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        this.util = new Utility();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_items);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "ambassador_script.ttf");

		Intent intentStart = getIntent();
		listid = intentStart.getStringExtra(List.EXTRA_LISTID);
		if(listid=="" || listid == null)
			listid = intentStart.getStringExtra(ScanResult.EXTRA_LISTID);
		
		Context context = (Context) this;
		progress = new ProgressDialog(context);
		
		String myurl = buildURL(listid);
		try {
			new Helper().execute(myurl);
		} catch (Exception e) {}

		scanBtn = (Button) findViewById(R.id.scan_button);
		scanBtn.setTypeface(font);

		listTxt = (TextView) findViewById(R.id.reg_name);
		
		listTxt.setText("List ID = "+listid);
		scanBtn.setOnClickListener(this);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	private String buildURL(String list_id) {
		String url ="";
		try{
			list_id = URLEncoder.encode(list_id, "UTF-8");
		} catch (Exception e){}
		
		url = "http://www.theinfiniteregistry.com/search.php?type=items&listid="+ list_id;
		Log.d(this.util.DEBUG_TAG, url);
		return url;
	}

	@Override
	public void onClick(View v) {
		// respond to clicks
		if (v.getId() == R.id.scan_button) {
			// scan
			IntentIntegrator scanIntegrator = new IntentIntegrator(this);
			scanIntegrator.initiateScan();
		}
	}
	
	public void updateView(JSONArray json) throws JSONException{
		TableLayout layout = (TableLayout) findViewById(R.id.items_lay);
		setContentView(layout);
		TextView reg_name = (TextView) findViewById(R.id.reg_name);
		TextView reg_count = (TextView) findViewById(R.id.items_tot);
        //reg_count.setText("No items were found for this registry.");
        Log.d(this.util.DEBUG_TAG, json.getJSONObject(0).toString());
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// retrieve scan result
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult.getContents() != null) {
			// we have a result
			String scanContent = scanResult.getContents();
			String scanFormat = scanResult.getFormatName();

			int scanLength = scanContent.length();

			String scanType = "None";
			if (scanLength == 12) {
				scanType = "upc";
			} else if (scanLength == 13) {
				scanType = "ean13";
			} else if (scanLength == 8) {
				scanType = "upc_e";
			} else {
				scanType = "Invalid UPC";
			}

			Intent intentSend = new Intent(this, ScanResult.class);
			intentSend.putExtra(this.util.EXTRA_UPC, scanType);
			intentSend.putExtra(this.util.EXTRA_CON, scanContent);
			intentSend.putExtra(this.util.EXTRA_FORMAT, scanFormat);
			intentSend.putExtra(this.util.EXTRA_LISTID, listid);
			startActivity(intentSend);

		} else {
			Toast toast = Toast.makeText(getApplicationContext(),
					"No scan data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.items, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    private class Helper extends AsyncTask<String, Void, String> {
		private Utility util;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Looking up items...");
			progress.setMessage("Searching...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				this.json = this.util.get_json(urls[0]);
				return "JSONObject created successfully";
			} catch (IOException e) {
				return "Failed to create JSONObject";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
            //only handle progress dialogs and context relevant methods in helper class.
            //All other methods should live in the Utility class and be referenced by this.util
			updateView(result);
		}
	}
}
